package main

import (
	"fmt"
	"github.com/openinx/vvlang/parser"
)

func main() {
	fmt.Printf("hello world\n")
	t, err := parser.Parse()
	if err != nil {
		fmt.Printf(err.Error())
	} else {
		fmt.Printf("%v\n", t)
	}
}
