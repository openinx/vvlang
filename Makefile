all: build 

build:
	go tool yacc -o parser/vv.go parser/vv.y
	gofmt -w parser/vv.go
	go build -o bin/vw cmd/vvlang.go

test:
	go test ./...

clean:
	rm -f parser/y.output vv.go y.output
