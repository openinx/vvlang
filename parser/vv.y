%{
package parser

func SetParseNode(yylex interface{}, node Node) {
  yylex.(*Tokenizer).ParseNode = node
}

func ForceEOF(yylex interface{}) {
  yylex.(*Tokenizer).ForceEOF = true
}

%}

%union{
    val      int
    str      string
    node     Node
    constant *ConstantNode
    variable *VariableNode
    operater *OperatorNode
}

%token LEX_ERROR
%token WHILE IF PRINT
%nonassoc IFX
%nonassoc ELSE

%left GE LE EQ NE '>' '<'
%left '+' '-'
%left '*' '/'
%nonassoc UMINUS


%start program


%token <str> VARIABLE
%token <val> INTEGER
%type <node> stmt stmt_list expr function

%%

program:
    function                
    { 
        SetParseNode(yylex, $1)
    }

function:
          function stmt         
          { 
            $$ = $1
          }
        | /* NULL */ { $$ = nil }

stmt:
          ';'                            
          { 
            $$ = NewOperateNode(SemicolunType, nil, nil)
          }
        | expr ';'                       
          { 
             $$ = $1
          }
        | PRINT expr ';'                 
          {
             $$ = NewOperateNode(PrintType, $2)
          }
        | VARIABLE '=' expr ';'          
          {  
             $$ = NewOperateNode(AssignType, &VariableNode{Value: $1}, $3)
          }
        | WHILE '(' expr ')' stmt        
          { 
             $$ = NewOperateNode(WhileType, $3, $5); 
          }
        | IF '(' expr ')' stmt 
          { 
             $$ = NewOperateNode(IfType, $3, $5); 
          }
        | IF '(' expr ')' stmt ELSE stmt 
          { 
             $$ = NewOperateNode(IfType, $3, $5, $7); 
          }
        | '{' stmt_list '}'             
          { 
             $$ = NewOperateNode(SemicolunType, $2); 
          }

stmt_list:
          stmt                  
          { 
            $$ = $1; 
          }
        | stmt_list stmt        
          { 
            $$ = NewOperateNode(SemicolunType, $1, $2)
          }

expr:
          INTEGER               
            { 
                $$ = &ConstantNode{Value: $1}
            }
        | VARIABLE              
            { 
                $$ = &VariableNode{Value: $1}
            }
        | '-' expr %prec UMINUS 
            { 
                $$ = NewOperateNode(UminusType, $2)
            }
        | expr '+' expr         
            { 
                $$ = NewOperateNode(AddType, $1, $3)
            }
        | expr '-' expr         
            { 
                $$ = NewOperateNode(MinusType, $1, $3)
            }
        | expr '*' expr         
            { 
                $$ = NewOperateNode(MultipType, $1, $3)
            }
        | expr '/' expr         
            { 
                $$ = NewOperateNode(DivType, $1, $3)
            }
        | expr '<' expr         
            { 
                $$ = NewOperateNode(LessThanType, $1, $3)
            }
        | expr '>' expr         
            { 
                $$ = NewOperateNode(GreaterThanType, $1, $3)
            }
        | expr GE expr          
            { 
                $$ = NewOperateNode(GreaterThanOrEqualType, $1, $3)
            }
        | expr LE expr          
            { 
                $$ = NewOperateNode(LessThanOrEqualType, $1, $3)
            }
        | expr NE expr          
            { 
                $$ = NewOperateNode(NotEqualType, $1, $3)
            }
        | expr EQ expr          
            { 
                $$ = NewOperateNode(EqualType, $1, $3)
            }
        | '(' expr ')'          
            { 
                $$ = $2
            }

%%
