package parser

type OpType int

const (
	WhileType              = OpType(0)  // while
	IfType                 = OpType(1)  // if
	PrintType              = OpType(2)  // print
	SemicolunType          = OpType(3)  // ;
	AssignType             = OpType(4)  // = ex: x= 4
	UminusType             = OpType(5)  // - ex: -4
	AddType                = OpType(6)  // +
	MinusType              = OpType(7)  // - ex: 4 - 5
	MultipType             = OpType(8)  // *
	DivType                = OpType(9)  // -
	LessThanType           = OpType(10) // <
	GreaterThanType        = OpType(11) // >
	GreaterThanOrEqualType = OpType(12) // <=
	LessThanOrEqualType    = OpType(13) // >=
	NotEqualType           = OpType(14) // !=
	EqualType              = OpType(15) // =
)

type Node interface {
	Print()
}

type ConstantNode struct {
	Value int
}

func (*ConstantNode) Print() {}

type VariableNode struct {
	Value string
}

func (*VariableNode) Print() {}

type OperatorNode struct {
	Node
	opType   OpType
	opNum    int
	operands []Node
}

func (*OperatorNode) Print() {}

func NewOperateNode(opType OpType, operands ...Node) Node {
	node := &OperatorNode{
		opType: opType,
		opNum:  len(operands),
	}
	node.operands = make([]Node, len(operands))
	for i := 0; i < len(operands); i++ {
		node.operands[i] = operands[i]
	}
	return node
}
