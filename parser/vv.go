//line parser/vv.y:2
package parser

import __yyfmt__ "fmt"

//line parser/vv.y:2
func SetParseNode(yylex interface{}, node Node) {
	yylex.(*Tokenizer).ParseNode = node
}

func ForceEOF(yylex interface{}) {
	yylex.(*Tokenizer).ForceEOF = true
}

//line parser/vv.y:14
type yySymType struct {
	yys      int
	val      int
	str      string
	node     Node
	constant *ConstantNode
	variable *VariableNode
	operater *OperatorNode
}

const LEX_ERROR = 57346
const WHILE = 57347
const IF = 57348
const PRINT = 57349
const IFX = 57350
const ELSE = 57351
const GE = 57352
const LE = 57353
const EQ = 57354
const NE = 57355
const UMINUS = 57356
const VARIABLE = 57357
const INTEGER = 57358

var yyToknames = []string{
	"LEX_ERROR",
	"WHILE",
	"IF",
	"PRINT",
	"IFX",
	"ELSE",
	"GE",
	"LE",
	"EQ",
	"NE",
	"'>'",
	"'<'",
	"'+'",
	"'-'",
	"'*'",
	"'/'",
	"UMINUS",
	"VARIABLE",
	"INTEGER",
}
var yyStatenames = []string{}

const yyEofCode = 1
const yyErrCode = 2
const yyMaxDepth = 200

//line parser/vv.y:158

//line yacctab:1
var yyExca = []int{
	-1, 1,
	1, -1,
	-2, 0,
}

const yyNprod = 28
const yyPrivate = 57344

var yyTokenNames []string
var yyStates []string

const yyLast = 176

var yyAct = []int{

	3, 29, 12, 28, 27, 5, 26, 11, 17, 18,
	13, 31, 25, 15, 16, 17, 18, 56, 32, 33,
	2, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 49, 30, 45, 46, 47, 1, 0, 0, 0,
	0, 0, 0, 8, 9, 6, 0, 0, 0, 0,
	0, 0, 0, 54, 55, 12, 0, 57, 0, 7,
	11, 4, 0, 13, 0, 10, 48, 21, 22, 24,
	23, 20, 19, 15, 16, 17, 18, 0, 0, 0,
	0, 0, 0, 53, 21, 22, 24, 23, 20, 19,
	15, 16, 17, 18, 0, 0, 0, 0, 0, 0,
	52, 21, 22, 24, 23, 20, 19, 15, 16, 17,
	18, 8, 9, 6, 0, 0, 0, 50, 0, 0,
	0, 0, 0, 12, 0, 0, 0, 7, 11, 4,
	0, 13, 0, 10, 21, 22, 24, 23, 20, 19,
	15, 16, 17, 18, 0, 0, 0, 51, 21, 22,
	24, 23, 20, 19, 15, 16, 17, 18, 0, 0,
	0, 44, 21, 22, 24, 23, 20, 19, 15, 16,
	17, 18, 0, 0, 0, 14,
}
var yyPact = []int{

	-1000, -1000, 106, -1000, -1000, 152, -15, -20, -22, -24,
	106, -1000, -15, -15, -1000, -15, -15, -15, -15, -15,
	-15, -15, -15, -15, -15, 138, -1000, -15, -15, -15,
	38, -1000, -1000, 91, -10, -10, -1000, -1000, -3, -3,
	-3, -3, -3, -3, -1000, 124, 74, 57, -1000, -1000,
	-1000, -1000, 106, 106, -1000, 8, 106, -1000,
}
var yyPgo = []int{

	0, 36, 0, 32, 5, 20,
}
var yyR1 = []int{

	0, 1, 5, 5, 2, 2, 2, 2, 2, 2,
	2, 2, 3, 3, 4, 4, 4, 4, 4, 4,
	4, 4, 4, 4, 4, 4, 4, 4,
}
var yyR2 = []int{

	0, 1, 2, 0, 1, 2, 3, 4, 5, 5,
	7, 3, 1, 2, 1, 1, 2, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3,
}
var yyChk = []int{

	-1000, -1, -5, -2, 23, -4, 7, 21, 5, 6,
	27, 22, 17, 25, 23, 16, 17, 18, 19, 15,
	14, 10, 11, 13, 12, -4, 21, 24, 25, 25,
	-3, -2, -4, -4, -4, -4, -4, -4, -4, -4,
	-4, -4, -4, -4, 23, -4, -4, -4, 28, -2,
	26, 23, 26, 26, -2, -2, 9, -2,
}
var yyDef = []int{

	3, -2, 1, 2, 4, 0, 0, 15, 0, 0,
	0, 14, 0, 0, 5, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 15, 0, 0, 0,
	0, 12, 16, 0, 17, 18, 19, 20, 21, 22,
	23, 24, 25, 26, 6, 0, 0, 0, 11, 13,
	27, 7, 0, 0, 8, 9, 0, 10,
}
var yyTok1 = []int{

	1, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	25, 26, 18, 16, 3, 17, 3, 19, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 23,
	15, 24, 14, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 27, 3, 28,
}
var yyTok2 = []int{

	2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
	12, 13, 20, 21, 22,
}
var yyTok3 = []int{
	0,
}

//line yaccpar:1

/*	parser for yacc output	*/

var yyDebug = 0

type yyLexer interface {
	Lex(lval *yySymType) int
	Error(s string)
}

const yyFlag = -1000

func yyTokname(c int) string {
	// 4 is TOKSTART above
	if c >= 4 && c-4 < len(yyToknames) {
		if yyToknames[c-4] != "" {
			return yyToknames[c-4]
		}
	}
	return __yyfmt__.Sprintf("tok-%v", c)
}

func yyStatname(s int) string {
	if s >= 0 && s < len(yyStatenames) {
		if yyStatenames[s] != "" {
			return yyStatenames[s]
		}
	}
	return __yyfmt__.Sprintf("state-%v", s)
}

func yylex1(lex yyLexer, lval *yySymType) int {
	c := 0
	char := lex.Lex(lval)
	if char <= 0 {
		c = yyTok1[0]
		goto out
	}
	if char < len(yyTok1) {
		c = yyTok1[char]
		goto out
	}
	if char >= yyPrivate {
		if char < yyPrivate+len(yyTok2) {
			c = yyTok2[char-yyPrivate]
			goto out
		}
	}
	for i := 0; i < len(yyTok3); i += 2 {
		c = yyTok3[i+0]
		if c == char {
			c = yyTok3[i+1]
			goto out
		}
	}

out:
	if c == 0 {
		c = yyTok2[1] /* unknown char */
	}
	if yyDebug >= 3 {
		__yyfmt__.Printf("lex %s(%d)\n", yyTokname(c), uint(char))
	}
	return c
}

func yyParse(yylex yyLexer) int {
	var yyn int
	var yylval yySymType
	var yyVAL yySymType
	yyS := make([]yySymType, yyMaxDepth)

	Nerrs := 0   /* number of errors */
	Errflag := 0 /* error recovery flag */
	yystate := 0
	yychar := -1
	yyp := -1
	goto yystack

ret0:
	return 0

ret1:
	return 1

yystack:
	/* put a state and value onto the stack */
	if yyDebug >= 4 {
		__yyfmt__.Printf("char %v in %v\n", yyTokname(yychar), yyStatname(yystate))
	}

	yyp++
	if yyp >= len(yyS) {
		nyys := make([]yySymType, len(yyS)*2)
		copy(nyys, yyS)
		yyS = nyys
	}
	yyS[yyp] = yyVAL
	yyS[yyp].yys = yystate

yynewstate:
	yyn = yyPact[yystate]
	if yyn <= yyFlag {
		goto yydefault /* simple state */
	}
	if yychar < 0 {
		yychar = yylex1(yylex, &yylval)
	}
	yyn += yychar
	if yyn < 0 || yyn >= yyLast {
		goto yydefault
	}
	yyn = yyAct[yyn]
	if yyChk[yyn] == yychar { /* valid shift */
		yychar = -1
		yyVAL = yylval
		yystate = yyn
		if Errflag > 0 {
			Errflag--
		}
		goto yystack
	}

yydefault:
	/* default state action */
	yyn = yyDef[yystate]
	if yyn == -2 {
		if yychar < 0 {
			yychar = yylex1(yylex, &yylval)
		}

		/* look through exception table */
		xi := 0
		for {
			if yyExca[xi+0] == -1 && yyExca[xi+1] == yystate {
				break
			}
			xi += 2
		}
		for xi += 2; ; xi += 2 {
			yyn = yyExca[xi+0]
			if yyn < 0 || yyn == yychar {
				break
			}
		}
		yyn = yyExca[xi+1]
		if yyn < 0 {
			goto ret0
		}
	}
	if yyn == 0 {
		/* error ... attempt to resume parsing */
		switch Errflag {
		case 0: /* brand new error */
			yylex.Error("syntax error")
			Nerrs++
			if yyDebug >= 1 {
				__yyfmt__.Printf("%s", yyStatname(yystate))
				__yyfmt__.Printf(" saw %s\n", yyTokname(yychar))
			}
			fallthrough

		case 1, 2: /* incompletely recovered error ... try again */
			Errflag = 3

			/* find a state where "error" is a legal shift action */
			for yyp >= 0 {
				yyn = yyPact[yyS[yyp].yys] + yyErrCode
				if yyn >= 0 && yyn < yyLast {
					yystate = yyAct[yyn] /* simulate a shift of "error" */
					if yyChk[yystate] == yyErrCode {
						goto yystack
					}
				}

				/* the current p has no shift on "error", pop stack */
				if yyDebug >= 2 {
					__yyfmt__.Printf("error recovery pops state %d\n", yyS[yyp].yys)
				}
				yyp--
			}
			/* there is no state on the stack with an error shift ... abort */
			goto ret1

		case 3: /* no shift yet; clobber input char */
			if yyDebug >= 2 {
				__yyfmt__.Printf("error recovery discards %s\n", yyTokname(yychar))
			}
			if yychar == yyEofCode {
				goto ret1
			}
			yychar = -1
			goto yynewstate /* try again in the same state */
		}
	}

	/* reduction by production yyn */
	if yyDebug >= 2 {
		__yyfmt__.Printf("reduce %v in:\n\t%v\n", yyn, yyStatname(yystate))
	}

	yynt := yyn
	yypt := yyp
	_ = yypt // guard against "declared and not used"

	yyp -= yyR2[yyn]
	yyVAL = yyS[yyp+1]

	/* consult goto table to find next state */
	yyn = yyR1[yyn]
	yyg := yyPgo[yyn]
	yyj := yyg + yyS[yyp].yys + 1

	if yyj >= yyLast {
		yystate = yyAct[yyg]
	} else {
		yystate = yyAct[yyj]
		if yyChk[yystate] != -yyn {
			yystate = yyAct[yyg]
		}
	}
	// dummy call; replaced with literal code
	switch yynt {

	case 1:
		//line parser/vv.y:45
		{
			SetParseNode(yylex, yyS[yypt-0].node)
		}
	case 2:
		//line parser/vv.y:51
		{
			yyVAL.node = yyS[yypt-1].node
		}
	case 3:
		//line parser/vv.y:54
		{
			yyVAL.node = nil
		}
	case 4:
		//line parser/vv.y:58
		{
			yyVAL.node = NewOperateNode(SemicolunType, nil, nil)
		}
	case 5:
		//line parser/vv.y:62
		{
			yyVAL.node = yyS[yypt-1].node
		}
	case 6:
		//line parser/vv.y:66
		{
			yyVAL.node = NewOperateNode(PrintType, yyS[yypt-1].node)
		}
	case 7:
		//line parser/vv.y:70
		{
			yyVAL.node = NewOperateNode(AssignType, &VariableNode{Value: yyS[yypt-3].str}, yyS[yypt-1].node)
		}
	case 8:
		//line parser/vv.y:74
		{
			yyVAL.node = NewOperateNode(WhileType, yyS[yypt-2].node, yyS[yypt-0].node)
		}
	case 9:
		//line parser/vv.y:78
		{
			yyVAL.node = NewOperateNode(IfType, yyS[yypt-2].node, yyS[yypt-0].node)
		}
	case 10:
		//line parser/vv.y:82
		{
			yyVAL.node = NewOperateNode(IfType, yyS[yypt-4].node, yyS[yypt-2].node, yyS[yypt-0].node)
		}
	case 11:
		//line parser/vv.y:86
		{
			yyVAL.node = NewOperateNode(SemicolunType, yyS[yypt-1].node)
		}
	case 12:
		//line parser/vv.y:92
		{
			yyVAL.node = yyS[yypt-0].node
		}
	case 13:
		//line parser/vv.y:96
		{
			yyVAL.node = NewOperateNode(SemicolunType, yyS[yypt-1].node, yyS[yypt-0].node)
		}
	case 14:
		//line parser/vv.y:102
		{
			yyVAL.node = &ConstantNode{Value: yyS[yypt-0].val}
		}
	case 15:
		//line parser/vv.y:106
		{
			yyVAL.node = &VariableNode{Value: yyS[yypt-0].str}
		}
	case 16:
		//line parser/vv.y:110
		{
			yyVAL.node = NewOperateNode(UminusType, yyS[yypt-0].node)
		}
	case 17:
		//line parser/vv.y:114
		{
			yyVAL.node = NewOperateNode(AddType, yyS[yypt-2].node, yyS[yypt-0].node)
		}
	case 18:
		//line parser/vv.y:118
		{
			yyVAL.node = NewOperateNode(MinusType, yyS[yypt-2].node, yyS[yypt-0].node)
		}
	case 19:
		//line parser/vv.y:122
		{
			yyVAL.node = NewOperateNode(MultipType, yyS[yypt-2].node, yyS[yypt-0].node)
		}
	case 20:
		//line parser/vv.y:126
		{
			yyVAL.node = NewOperateNode(DivType, yyS[yypt-2].node, yyS[yypt-0].node)
		}
	case 21:
		//line parser/vv.y:130
		{
			yyVAL.node = NewOperateNode(LessThanType, yyS[yypt-2].node, yyS[yypt-0].node)
		}
	case 22:
		//line parser/vv.y:134
		{
			yyVAL.node = NewOperateNode(GreaterThanType, yyS[yypt-2].node, yyS[yypt-0].node)
		}
	case 23:
		//line parser/vv.y:138
		{
			yyVAL.node = NewOperateNode(GreaterThanOrEqualType, yyS[yypt-2].node, yyS[yypt-0].node)
		}
	case 24:
		//line parser/vv.y:142
		{
			yyVAL.node = NewOperateNode(LessThanOrEqualType, yyS[yypt-2].node, yyS[yypt-0].node)
		}
	case 25:
		//line parser/vv.y:146
		{
			yyVAL.node = NewOperateNode(NotEqualType, yyS[yypt-2].node, yyS[yypt-0].node)
		}
	case 26:
		//line parser/vv.y:150
		{
			yyVAL.node = NewOperateNode(EqualType, yyS[yypt-2].node, yyS[yypt-0].node)
		}
	case 27:
		//line parser/vv.y:154
		{
			yyVAL.node = yyS[yypt-1].node
		}
	}
	goto yystack /* stack new state and value */
}
